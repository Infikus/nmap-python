import unittest
import nmap_xml_to_html

class TestNmapXmlToHtml(unittest.TestCase):
  def test_parse_nmap_xml(self):
    file1 = 'tests_xml/host_with_ports.xml'
    res = nmap_xml_to_html.parse_nmap_xml(file1)
    self.assertEqual(res['cmd'], 'nmap cmd')
    self.assertEqual(res['start_date'], 'nmap startstr')
    self.assertEqual(len(res['hosts']), 1)
    '''Check hosts information'''
    host = res['hosts'][0]
    self.assertEqual(host['address'], ['123.123.123.123'])
    self.assertEqual(host['hostnames'], [{ 'name':'hostname1', 'type': 'user' }, { 'name': 'hostname2', 'type': 'PTR' }])
    self.assertEqual(host['status'], 'up')
    '''Check extraports'''
    self.assertEqual(len(host['extraports']), 2)
    self.assertEqual(host['extraports'][0], { 'state': 'closed', 'count': '65457' })
    self.assertEqual(host['extraports'][1], { 'state': 'filtered', 'count': '66' })
    '''Check ports'''
    self.assertEqual(len(host['ports']), 3)
    self.assertEqual(host['ports'][0]['port'], '22')
    self.assertEqual(host['ports'][0]['protocol'], 'tcp')
    self.assertEqual(host['ports'][0]['state'], 'open')
    self.assertEqual(host['ports'][0]['scripts'], [])

    file2 = 'tests_xml/host_without_ports.xml'
    res = nmap_xml_to_html.parse_nmap_xml(file2)
    self.assertEqual(res['cmd'], 'nmap cmd')
    self.assertEqual(res['start_date'], 'nmap startstr')
    self.assertEqual(len(res['hosts']), 1)
    '''Check hosts information'''
    host = res['hosts'][0]
    self.assertEqual(host['address'], ['123.123.123.123'])
    self.assertEqual(host['hostnames'], [])
    self.assertEqual(host['status'], 'down')

    file3 = 'tests_xml/no_hosts.xml'
    res = nmap_xml_to_html.parse_nmap_xml(file3)
    self.assertEqual(res['cmd'], 'nmap cmd')
    self.assertEqual(res['start_date'], 'nmap startstr')
    self.assertEqual(len(res['hosts']), 0)

    file4 = 'tests_xml/multiple_hosts.xml'
    res = nmap_xml_to_html.parse_nmap_xml(file4)
    self.assertEqual(res['cmd'], 'nmap cmd')
    self.assertEqual(res['start_date'], 'nmap startstr')
    self.assertEqual(len(res['hosts']), 3)
    '''Check hosts information'''
    host1 = res['hosts'][0]
    self.assertEqual(host1['address'], ['123.123.123.123'])
    self.assertEqual(host1['hostnames'], [{ 'name':'hostname1', 'type': 'user' }, { 'name': 'hostname2', 'type': 'PTR' }])
    self.assertEqual(host1['status'], 'up')
    host2 = res['hosts'][1]
    self.assertEqual(host2['address'], ['123.123.123.123'])
    self.assertEqual(host2['hostnames'], [{ 'name':'hostname1', 'type': 'user' }, { 'name': 'hostname2', 'type': 'PTR' }])
    self.assertEqual(host2['status'], 'up')
    host3 = res['hosts'][2]
    self.assertEqual(host3['address'], ['123.123.123.123'])
    self.assertEqual(host3['hostnames'], [])
    self.assertEqual(host3['status'], 'down')

if __name__ == '__main__':
  unittest.main()
