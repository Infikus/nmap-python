from __future__ import print_function
import re, socket, argparse, sys

def parse_args():
  parser = argparse.ArgumentParser()
  parser.add_argument('-iL', help='file containing a list of targets', nargs='?', type=argparse.FileType('r'))
  parser.add_argument('targets', help='targets to scan', nargs='*')

  # NSE scripts
  parser.add_argument(
    '--http-methods',
    '-hm',
    help='Finds out what options are supported by an HTTP server', action='store_true',
  )
  parser.add_argument(
    '--http-enum',
    '-he',
    help='Performs Brute Force on a server path in order to discover web applications in use', action='store_true',
  )
  parser.add_argument(
    '--script',
    '-s',
    help='Runs a script scan',
    nargs='*',
  )

  return check_args(parser.parse_args(), parser)

'''
  Verify if parsed arguments are valid.
  If there are no targets in the arguments, the help is displayed.
  If there are no valid targets, returns None
  Else returns the args.
  args.targets will contain all the targets (including thoses that were in a file)
'''
def check_args(args, parser):
  '''if there is no input file (with -iL)'''
  if args.iL is None and len(args.targets) == 0:
    parser.print_help()
    return None
  else:
    args.targets = check_target_list(args.targets)
    if args.iL is not None:
      args.targets += check_file(args.iL)
      args.iL.close()
      args.iL = None

    if len(args.targets) == 0:
      print('ERROR: There is no valid target to scan', file=sys.stderr)
      return None

  return args

'''
  Checks if a file contains valid entries
  Returns a list of targets
'''
def check_file(file):
  file_content = file.read()
  targets = re.split('\t|\n| ', file_content)
  return check_target_list(targets)

'''
  Checks a list of targets.
  If a target is not valid, it will be deleted from the list
'''
def check_target_list(targets):
  valid_targets = []
  for t in targets:
    stripped_t = t.strip()
    # ignore empty strings
    if stripped_t == '':
      continue
    is_valid = validate_input(stripped_t)
    if is_valid != 0:
      if is_valid == IS_HOST:
        # try to resolve hostnames
        try:
          socket.gethostbyname_ex(stripped_t.split('/')[0])
        except:
          print('WARNING: Cannot resolve {0}. It will be ignored.'.format(stripped_t), file=sys.stderr)
          continue;
      valid_targets.append(stripped_t)
    else:
      print('WARNING: {0} is not a valid target. It will be ignored.'.format(stripped_t), file=sys.stderr)

  return valid_targets


'''
  Checks if the arg is a valid hostname (does not try to resolve the name)
'''
def is_host(host):
  if len(host) > 255:
    return False
  if host[-1] == ".":
    host = host[:-1] # strip exactly one dot from the right, if present
  allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
  return all(allowed.match(x) for x in host.split("."))


'''
  Checks if the arg is a valid ipv4
'''
def is_ip4(ip):
  try:
    socket.inet_pton(socket.AF_INET, ip)
    return True
  except:
    return False

'''
  Checks if the arg is a valid ipv6
'''
def is_ip6(ip):
  try:
    socket.inet_pton(socket.AF_INET6, ip)
    return True
  except:
    return False


IS_IP4 = 1
IS_IP6 = 2
IS_HOST = 3
'''
  Checks if a string is either an ipv4, an ipv6 or a hostname
  Returns 0 if the string is not valid, else returns either IS_IP4, IS_IP6 or IS_HOST
'''
def validate_input(string):
  '''check if it is a cidr'''
  cidr = string.split('/')

  '''There cannot be more than one / '''
  if len(cidr) > 2:
    return False
  '''Check if the mask is correct (between 0 and 32)'''
  if len(cidr) == 2:
    try:
      mask = int(cidr[1])
      if mask < 0 or mask > 32:
        return False
    except:
      return False

  if is_ip4(cidr[0]): return IS_IP4
  if is_ip6(cidr[0]): return IS_IP6
  if is_host(cidr[0]): return IS_HOST

  return 0

