#!/usr/bin/env python3

from __future__ import print_function
import subprocess, shlex, time, sys, re, argparse, os

import nmap_xml_to_html
import inputs
from tui import Tui

class Nmap:
  '''
  Args:
    targets: a list of targets
    options (not implemented): options to add to the nmap command
    tui: a Tui instance that will be used to display information
  '''
  def __init__(self, targets, options=None, tui=Tui()):
    self.targets = targets
    self.options = options
    self.tui = tui
    self.__init_regex()

  '''compile regexes to go faster'''
  def __init_regex(self):
    self.regex_discovery = re.compile(r'discovered.*', re.I)
    self.regex_progress = re.compile(
      r'(?P<step>.*) Timing[^0-9]*(?P<progress>[0-9.]*)%[^(]*(\((?P<remaining>.* remaining)\))?',
      re.IGNORECASE
    )
    self.regex_nmap_done = re.compile(r'nmap done.*', re.I)


  '''Start a scan'''
  def start(self):
    # run nmap
    process = self.__run()
    # read nmap output and display a progress bar with some information
    self.__analyze_output()
    # parse the xml output
    data = nmap_xml_to_html.parse_nmap_xml('data.xml')
    # and generate an html file
    nmap_xml_to_html.generate_html_file_from_hosts(data)
    # remove data.xml
    try:
      os.remove('data.xml')
    except:
      print('Cannot remove data.xml', file=sys.stderr)
    

  '''Run the command and return a process'''
  def __run(self):
    '''Select targets'''
    targets = ' '.join(self.targets)

    options = '-p- -sV -T5 -v --stats-every 1s -oX data.xml '
    if self.options is not None:
      if self.options.http_methods:
        options += '--script http-methods '
      if self.options.http_enum:
        options += '--script http-enum '
      if self.options.script:
        options += ''.join('--script {0} '.format(s) for s in self.options.script)

    command = shlex.split('nmap ' + options + targets)

    self.process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE
        )

  '''
    Read the output of the nmap command.
    Each line that is read is parsed in order to display only the useful information
  '''
  def __analyze_output(self):
    for line in self.__read_output():
      line = line.decode('utf-8').strip()
      self.__check_progress(line)
      self.__check_discovery(line)
      self.__check_nmap_done(line)
    self.tui.erase_progress_bar()


  '''
    Read all lines output by nmap and yield the value
  '''
  def __read_output(self):
    while True:
      output = self.process.stdout.readline()
      '''if there is nothing to show and the process is not running'''
      if not output and self.process.poll() is not None:
        break;
      if output:
        yield output

  '''
    Parse a line to determine if it contains information about the scan progress
  '''
  def __check_progress(self, line):
    progress_match = re.match(self.regex_progress, line)
    if progress_match is not None: self.__on_progress(progress_match)

  '''
    Parse a line to determine if it contains information about ports that have been discovered
  '''
  def __check_discovery(self, line):
    if re.match(self.regex_discovery, line) is not None:
      self.tui.print_before_progress_bar(line)

  '''
    Parse a line to determine if it indicates that nmap has finished its work
  '''
  def __check_nmap_done(self, line):
    if re.match(self.regex_nmap_done, line) is not None:
      self.tui.print_before_progress_bar(line)

  '''
    This function is called when the command output information about the scan progress.
    It will show/update a progress bar to display clear information to the user
  '''
  def __on_progress(self, data):
    remaining = '' if data.group('remaining') is None else data.group('remaining')
    self.tui.full_width_progress_bar(data.group('step'), float(data.group('progress')), remaining)
      
def main():
  # Load command arguments
  args = inputs.parse_args()
  if args is None: return

  # Start the name scan
  Nmap(args.targets, args).start()

if __name__ == '__main__':
  main()
