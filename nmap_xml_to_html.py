from __future__ import print_function
import sys, cgi, random
import xml.etree.ElementTree as ET

'''
Generate an html file using a list of host info

Args:
  data (should be the output of the parse_nmap_xml function):
    {
      'cmd': (string),
      'start_date': (string),
      'hosts': (list)
    }
'''
def generate_html_file_from_hosts(data):
  html_file_name = 'html/' + data['start_date'].replace(' ', '_') + '.html'
  print('Generating ' + html_file_name + '...')

  '''Open html templates'''
  try:
    template = open('template.html', 'r')
    template_host = open('template_host.html', 'r')
  except FileNotFoundError:
    print('templates not found. Cannot generate an html result without template.')
    return
  except:
    print('An error occured when reading templates. Cannot generate an html result.')

  html_template = template.read()
  html_host_template = template_host.read()

  '''template files are no more useful'''
  template.close()
  template_host.close()

  '''Add the title and the nmap command used to get the result'''
  page_title = 'Port scan report - {0}'.format(data['start_date'])
  html = html_template.replace('___TITLE___', page_title)
  html = html.replace('___CMD___', data['cmd'])

  '''Browse all hosts to generate corresponding html'''
  html_hosts = []
  for host in data['hosts']:
    if len(host['address']) == 0: # It should never happened if the xml file is valid
      print('Invalid host found. Ignored', sys) 
      continue
    '''Down hosts are not shown'''
    if host['status'] == 'down':
      print('{0} is down. It will be ignored in the html report'.format(host['address'][0]), file=sys.stderr)
      continue
    html_host = __add_host_title_to_html(html_host_template, host)
    html_host = __add_hostnames_to_html(html_host, host)
    html_host = __add_extraports_to_html(html_host, host)
    html_host = __add_ports_to_html(html_host, host)

    html_hosts.append(html_host)

  html_hosts = ''.join(html_hosts)
  html = html.replace('___HOSTS___', html_hosts)
  

  try:
    file = open(html_file_name, 'w')
  except:
    print('cannot open ' + html_file_name)
    return
  
  file.write(html)
  file.close()

  print(html_file_name + ' has been generated')
  return html_file_name


def __add_host_title_to_html(html, host):
  address = host['address']
  status = host['status']
  if len(address) == 1:
    address_html = '{0}'.format(address[0])
  else:
    address_html = '/'.join(address)

  if status != 'undefined':
    address_html += ' ({0})'.format(status)

  return html.replace('___HOST___', address_html)

def __add_hostnames_to_html(html, host):
  hostnames = host['hostnames']
  hostnames_html = ''.join([
    '<li>{0} ({1})</li>'.format(name['name'], name['type'])
    for name in hostnames
  ])
  hostnames_html = '<ul>{0}</ul>'.format(hostnames_html)

  return html.replace('___HOSTNAMES___', hostnames_html)

def __add_extraports_to_html(html, host):
  extraports = host['extraports']
  extraports_html = '<br/>'.join(['{0} ports are not shown below because there are in state: {1}'.format(e['count'], e['state']) for e in extraports])

  return html.replace('___EXTRAPORTS___', extraports_html)

def __add_ports_to_html(html, host):
  ports = host['ports']
  if len(ports) == 0:
    ports_html = '<tr><td colspan="8">There is no ports to show</td></tr>'
  else:
    ports_html = ''.join([__generate_port_html(p, host) for p in ports])

  return html.replace('___PORTS___', ports_html)

def __generate_port_html(port, host):
  service = port['service']

  '''Create a table row to describe a port'''
  port_html = '<td>{0}</td>'.format(port['port'])
  port_html += '<td>{0}</td>'.format(port['protocol'])
  port_html += '<td>{0}</td>'.format(port['state'])

  port_html += '<td>{0}</td>'.format(service.get('name', ''))
  port_html += '<td>{0}</td>'.format(service.get('product', ''))
  port_html += '<td>{0}</td>'.format(service.get('version', ''))
  port_html += '<td>{0}</td>'.format(service.get('extrainfo', ''))

  show_scripts_message = 'Click to show/hide scripts' if len(port['scripts']) > 0 else ''
  port_html += '<td>{0}</td>'.format(show_scripts_message)

  '''If there are some scripts, we need to add new rows to the table'''
  # Generate an id used to show scripts. The random.random is juste there to avoid collisions.
  scripts_id = 'scripts-{0}-{1}-{2}'.format(host['address'][0], port['port'], str(random.random()))
  if len(port['scripts']) > 0:
    # Replace '\r\n' and '\n' by <br /> in script content
    scripts = [
      { 'id': s['id'], 'output': s['output'].replace('\r\n', '<br />').replace('\n', '<br />') }
      for s in port['scripts']]

    # Generate a table content with all scripts
    scripts_html = ''.join([
      '<tr><td>|</td><td>{0}</td><td colspan="6">{1}</td>'.format(s['id'], s['output'])
      for s in scripts])
    port_html = '<tr class="collapse-button" data-target="{1}">{0}</tr>'.format(port_html, scripts_id)
  else:
    scripts_html = ''
    port_html = '<tr>{0}</tr>'.format(port_html)

  return '<tbody>{0}</tbody><tbody id="{1}" class="script collapse">{2}</tbody>'.format(port_html, scripts_id, scripts_html)

'''
This function parses the XML output of a nmap command

Args:
  file (string): name of the file to parse
'''
def parse_nmap_xml(file):
  try:
    tree = ET.parse(file)
    root = tree.getroot()
  except:
    print("Cannot parse data.xml (invalid xml file).", file=sys.stderr)
    sys.exit(1)

  if root is None:
    print("Cannot parse data.xml (invalid xml file).", file=sys.stderr)
    sys.exit(1)

  cmd = root.attrib.get('args', 'No command')
  start_date = root.attrib.get('startstr', '')

  hosts = [__parse_host(e) for e in root.iter('host')]

  return {
    'cmd': cmd,
    'start_date': start_date,
    'hosts': hosts,
  }


'''
  Parse an host element of the XML output of a nmap command
'''
def __parse_host(host_element):
  host = {}
  '''Read address and hostnames'''
  status = host_element.find('status')
  if status is None:
    host['status'] = 'undefined'
  else:
    host['status'] = status.attrib.get('state', 'undefined')

  host['address'] = [e.attrib['addr'] for e in host_element.findall('address')]
  host['hostnames'] = [{'name': e.attrib.get('name', ''), 'type': e.attrib.get('type', '')} for e in host_element.iter('hostname')]

  '''Read data about scanned ports'''
  ports_element = host_element.find('ports')
  if ports_element is None:
    host['ports'] = []
    host['extraports'] = []
    return host

  # extraports contains information about ports witout result
  extraports = [__parse_extraports(e) for e in ports_element.findall('extraports')]
  host['extraports'] = extraports

  '''parse all open/filtered ports'''
  ports = [__parse_port(e) for e in ports_element.findall('port')]
  
  host['ports'] = ports
  return host

'''
  Parse an extraports element of the XML output of a nmap command
'''
def __parse_extraports(extraports_element):
  return {
    'state': extraports_element.attrib.get('state', 'undefined'),
    'count': extraports_element.attrib.get('count', '0'),
  }

'''
  Parse an port element of the XML output of a nmap command
'''
def __parse_port(port_element):
  attr = port_element.attrib

  '''Read the state of the port (open/filtered/close)'''
  state_element = port_element.find('state')
  state_attr = {} if state_element is None else state_element.attrib

  '''Read the info about the service running on this port'''
  service_element = port_element.find('service')
  service = __parse_service(service_element)
  
  '''Parse scripts'''
  script_elements = port_element.findall('script')
  scripts = [__parse_script(s) for s in script_elements]

  port = {
    'port': attr.get('portid', 'undefined'),
    'protocol': attr.get('protocol', 'undefined'),
    'state': state_attr.get('state', 'undefined'),
    'service': service,
    'scripts': scripts,
  }
  return port

def __parse_service(service_element):
  el = service_element if service_element is not None else {}

  return {
    'name': el.get('name', 'undefined'),
    'product': el.get('product', ''),
    'version': el.get('version', ''),
    'version': el.get('version', ''),
  }

def __parse_script(script_element):
  attr = script_element.attrib

  id = attr.get('id', '')
  # We need to espace some characters here because a script output can contain valid html
  output = cgi.escape(attr.get('output', ''))

  return {
    'id': id,
    'output': output,
  }
  
