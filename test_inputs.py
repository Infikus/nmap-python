import unittest
import inputs

class TestInputs(unittest.TestCase):
  def test_is_ip4(self):
    self.assertTrue(inputs.is_ip4('123.123.123.123'))
    self.assertTrue(inputs.is_ip4('1.12.123.1'))
    self.assertFalse(inputs.is_ip4('123.123.123.123.1'))
    self.assertFalse(inputs.is_ip4('123.123.123.1234'))
    self.assertFalse(inputs.is_ip4('256.123.123.123'))
    self.assertFalse(inputs.is_ip4('qsd.qsd.qsd.qsd'))
    self.assertFalse(inputs.is_ip4('azerty'))

  def test_is_ip6(self):
    self.assertTrue(inputs.is_ip6('2001:db8:0:85a3::ac1f:8001'))
    self.assertTrue(inputs.is_ip6('2001:db8:0:85a3:0:0:ac1f:8001'))
    self.assertTrue(inputs.is_ip6('2001:db8:0:85a3::ac1f:8001'))
    self.assertFalse(inputs.is_ip6('2001:db8::85a3::ac1f:8001'))
    self.assertFalse(inputs.is_ip6('2001:db8:1234:85a3:ac1f:8001'))
    self.assertFalse(inputs.is_ip6('2001:db8::85a3::ac1f:8001:1234'))
    self.assertFalse(inputs.is_ip6('256.123.123.123'))
    self.assertFalse(inputs.is_ip6('qsd:qsd:qsd:qsd:qsd:qsd'))
    self.assertFalse(inputs.is_ip6('azerty'))

  def test_is_host(self):
    self.assertTrue(inputs.is_host('google.com'))
    self.assertTrue(inputs.is_host('abc.def.12.host.google.com'))
    self.assertFalse(inputs.is_host('google.com-'))
    self.assertFalse(inputs.is_host(64*'a'))
    self.assertFalse(inputs.is_host((60*'a')*10))

  def test_validate_input(self):
    self.assertTrue(inputs.validate_input('123.123.123.123'))
    self.assertTrue(inputs.validate_input('123.123.123.123/12'))
    self.assertFalse(inputs.validate_input('123.123.123.123/34'))

    self.assertTrue(inputs.validate_input('2001:db8:0:85a3::ac1f:8001'))
    self.assertTrue(inputs.validate_input('2001:db8:0:85a3::ac1f:8001/21'))
    self.assertFalse(inputs.validate_input('2001:db8:0:85a3::ac1f:8001/-2'))
    self.assertFalse(inputs.validate_input('2001:db8:0:85a3::ac1f:8001/qs'))

  def test_check_target_list(self):
    l1 = ['123.123.123.123', 'google.com']
    self.assertEqual(inputs.check_target_list(l1), l1)

    l2 = ['123.123.123.123', 'google.com/failed']
    l2_valid = ['123.123.123.123']
    self.assertEqual(inputs.check_target_list(l2), l2_valid)

    l3 = ['not_valid/56', 'still_not_right....']
    self.assertEqual(inputs.check_target_list(l3), [])

    self.assertEqual(inputs.check_target_list([]), [])


if __name__ == '__main__':
  unittest.main()
