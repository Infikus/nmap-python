Scanner de port
---------------

Ce script a pour but de fournir un moyen simple d'effectuer un scan de port.
Ce scanner s'appuie sur l'outil nmap qui est un outil très utilisé pour des scans de port mais qui peut s'avérer compliqué à utiliser.

Le script se lance avec "python port_scanner.py"
L'utilisation la plus simple de ce script consiste à fournir en arguments la liste des cibles à scanner. Par défaut, ce script scan tous les ports des cibles pour détecter les ports ouverts ainsi que les services qui y tournent et leur version.

Voici la liste des options disponibles (également disponible avec l'option -h) :
  -iL                   Permet d'utiliser un fichier contenant une liste de cibles
  --http-methods, -hm   Lance le script http-methods qui retourne la liste des options supportées par un serveur HTTP
  --http-enum, -he      Lance le script http-enum qui essaye de trouver des applications tournant sur un serveur
  --script              Lance un script NSE

Lorsque le scan est effectué, la commande affiche le nom du fichier html contenant les résultats. Ces résultats sont stockés dans le dossier html, pour pouvoir les conserver et les consulter ultérieurement.

Les scans peuvent être assez long, notamment si des scripts NSE sont utilisés. Pour que ce temps soit le plus court possible, l'option -T4 de nmap est utilsé (-T5 est plus rapide mais pas toujours conseillé car très violent en terme de quantité de requêtes envoyés).


Tests
-----
J'ai ajouté quelques tests unitaires pour la validation des entrées de l'utilisateur et le parsing du fichier xml.
Ces tests peuvent être lancés avec les commandes suivantes :
python -m unittest -v test_inputs
python -m unittest -v test_xml_to_html
