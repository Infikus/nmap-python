import sys, os

'''
  This class helps to work with a progress bar
  It allows you to simply display a progress bar while allowing you to display text above it
'''
class Tui:
  def __init__(self):
    # These variables contain information about the progress bar that is shown
    self.show_progress_bar = False
    self.progress_bar_title = ''
    self.progress_bar_progress = 0
    self.progress_bar_text_after = ''
    self.progress_bar_length = 0

  '''
    display a progress bar

    progress must be contain between 0 and 100
    
    output format: [title] [#######---------] [text_after]
  '''
  def progress_bar(self, title, progress, text_after='', length=30):
    # Update information about the progress bar
    self.show_progress_bar = True
    self.progress_bar_title = title
    self.progress_bar_progress = progress
    self.progress_bar_text_after = text_after
    self.progress_bar_length = length

    # Calculate the number of # characters to show
    nb_block = int(round(progress*length/100))
    msg = '{0}: [{1}{2}] {3}% {4}\r'.format(
        title,
        '#'*nb_block,
        '-'*(length - nb_block),
        progress,
        text_after,
        )

    # write the progress bar
    sys.stdout.write(msg)
    sys.stdout.flush()

  '''
    display a full width progress bar

    progress must be contain between 0 and 100
    
    output format: [title] [#######---------] [text_after]
  '''
  def full_width_progress_bar(self, title, progress, text_after=''):
    rows, columns = os.popen('stty size', 'r').read().split()
    bar_length = int(columns) - len(title) - len(text_after) - len(str(progress)) - 7
    self.progress_bar(title, progress, text_after, bar_length)

  '''
    display the last progress bar
    Usefull when a text is write over the progress bar and you need to draw it again
  '''
  def last_progress_bar(self):
    self.progress_bar(
        self.progress_bar_title,
        self.progress_bar_progress,
        self.progress_bar_text_after,
        self.progress_bar_length
        )

  '''
    Replace the current progress bar with spaces
  '''
  def erase_progress_bar(self):
    if not self.show_progress_bar: return
    print(' '*self.__len_progress_bar())

  '''
    Print a text before the progress bar

    Basically the text is write over the progress bar. If a part of the progress bar is not covered by the text, spaces are added
    Then the progress bar is printed in the next line
  '''
  def print_before_progress_bar(self, text):
    if self.show_progress_bar:
      nb_chars_to_fill = max(0, self.__len_progress_bar() - len(text))
      print(text + ' '*nb_chars_to_fill)
      self.last_progress_bar()
    else:
      print(text)

  '''
    Returns the number of characters needed to print the progress bar
  '''
  def __len_progress_bar(self):
    return len(self.progress_bar_title) + len(str(self.progress_bar_progress)) + len(self.progress_bar_text_after) + self.progress_bar_length + 7

